//
//  AgentProfileDetail.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 8/6/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import SwiftUI

struct AgentProfileDetail: View {

    @Environment(\.editMode) var mode
    @State var profile : AgentProfile = currentUser
    @State var draftProfile : AgentProfile = currentUser
    @State var showBrokerSheet = false
    
    var editBrokerInfo : some View {
        Button(action: { self.showBrokerSheet.toggle() }) {
            Text("Broker's Info")
                .font(.subheadline)
                .fontWeight(.semibold)
                .multilineTextAlignment(.center)
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                if self.mode?.wrappedValue == .active {
                    Button(action: {
                        self.profile = self.draftProfile
                        self.mode?.animation().wrappedValue = .inactive
                    }) {
                        Text("Done")
                    }
                }
                Form {
                    Section {
                        if self.mode?.wrappedValue == .inactive {
                            
                            ProfileEditor(profile: self.$draftProfile)
                                .onDisappear {
                                self.draftProfile = self.profile
                            }
                        } else {
                            
                            ProfileSummary(profile: self.profile)
                                .padding(.all)
                        }
                        
                    }
                
                }
                
                self.editBrokerInfo
            }
            
            .navigationBarTitle(Text("\(profile.firstName)'s Agent Profile"))
                .sheet(isPresented: $showBrokerSheet) {
                    AgentProfileDetail_Broker(curBroker: self.profile.broker)
                    Button(action: { self.showBrokerSheet.toggle() }) {
                        Text("Done")
                            .font(.headline)
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                    }
            }
            .navigationBarItems(trailing:
            EditButton())
            
        }
    }
}

#if DEBUG
struct AgentProfileDetail_Previews: PreviewProvider {
    static var previews: some View {
        AgentProfileDetail()
    }
}
#endif

struct AgentProfileDetail_Broker : View {
    @State var curBroker : AgentProfile.Broker
    
    var body: some View {
        Form {
            Section {
                HStack {
                    Text("Broker Lic. Number")
                    Spacer()
                    TextField("Broker Lic. Number", text: $curBroker.licNum)
                }
                HStack {
                    Text("Firm Name")
                    Spacer()
                    TextField("Firm Name", text: $curBroker.firmName)
                }
                HStack {
                    Text("Phone Number")
                    Spacer()
                    TextField("Phone Number", text: $curBroker.phoneNum)
                }
                HStack {
                    Text("Principal Broker")
                    Spacer()
                    TextField("Principal Broker", text: $curBroker.principalName)
                }
            }
            .allowsTightening(true)
            .padding(EdgeInsets())
            Section {
                TextField("Address 1", text: $curBroker.address.line_1)
                TextField("Address 2", text: $curBroker.address.line_2)
                HStack {
                    TextField("City", text: $curBroker.address.City)
                    TextField("State", text: $curBroker.address.State)
                    TextField("Zip", text: $curBroker.address.Zip)
                }
            }
            .allowsTightening(true)
        }
        .navigationBarTitle(Text(curBroker.firmName), displayMode: .inline)
    }
}


#if DEBUG
struct AgentProfileDetail_Broker_Previews: PreviewProvider {
    static var previews: some View {
        AgentProfileDetail_Broker(curBroker: AgentProfile.Broker())
    }
}
#endif


struct ProfileEditor : View {
    @Binding var profile : AgentProfile
    
    var body: some View {
        VStack {
            Form {
                Section {
                    
                    HStack {
                        TextField("First Name", text: $profile.firstName)
                            .padding(.trailing)
                        TextField("MI", text: $profile.middleName)
                            .padding(.leading)
                            .padding(.trailing)
                        TextField("Last Name", text: $profile.lastName)
                            .padding(.leading)
                    }
                    
                    HStack {
                        TextField("License Number", text: $profile.license_number)
                        TextField("Commission % Rate:" , text: $profile.commission_rate)
                    }
                    
                    HStack {
                        TextField("Email", text: $profile.email_address)
                        TextField("Mobile #", text: $profile.mobile_phone_number)
                    }
                    
                    
                }
                .allowsTightening(true)
            }
        }
    }
}

struct ProfileSummary : View {
    var profile : AgentProfile
    
    var body: some View {
        
        Section {
            HStack {
                Text("First Name")
                    .font(.headline)
                Divider()
                Spacer()
                Text("\(profile.firstName)")
            }
            HStack {
                Text("MI")
                .font(.headline)
                Divider()
                Spacer()
                Text("\(profile.middleName)")
            }
            HStack {
                Text("Last Name")
                .font(.headline)
                Divider()
                Spacer()
                Text("\(profile.lastName)")
            }
            HStack {
                Text("License Number")
                .font(.headline)
                Divider()
                Spacer()
                Text("\(profile.license_number)")
            }
            HStack {
                Text("Commission % Rate")
                .font(.headline)
                Divider()
                Spacer()
                Text("\(profile.commission_rate)")
            }
            HStack {
                Text("Email")
                .font(.headline)
                Divider()
                Spacer()
                Text("\(profile.email_address)")
            }
            HStack {
                Text("Mobile #")
                .font(.headline)
                Divider()
                Spacer()
                Text("\(profile.mobile_phone_number)")
            }
        }
    }
}
