//
//  Library.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/25/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import SwiftUI

struct Library: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello World!"/*@END_MENU_TOKEN@*/)
    }
}

#if DEBUG
struct Library_Previews: PreviewProvider {
    static var previews: some View {
        Library()
    }
}
#endif
//
//struct Integrated_PSCGridViewController : UIViewControllerRepresentable {
//
//    func makeUIViewController(context: UIViewControllerRepresentableContext<Integrated_PSCGridViewController>) -> PSPDFViewController {
//        return PSCGridViewController()
//    }
//
//    func updateUIViewController(_ uiViewController: PSPDFViewController, context: UIViewControllerRepresentableContext<Integrated_PSCGridViewController>) {
//
//    }
//}
