//
//  DocumentLibrary.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/24/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import SwiftUI

struct DocumentLibrary: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello World!"/*@END_MENU_TOKEN@*/)
    }
}

#if DEBUG
struct DocumentLibrary_Previews: PreviewProvider {
    static var previews: some View {
        DocumentLibrary()
    }
}
#endif

//struct Integrated_PSPDFViewController : UIViewControllerRepresentable {
//    var currentPDF : PSPDFDocument?
//    
//    /*func makeCoordinator() -> Coordinator {
//    Coordinator(self)
//    }*/
//    
//    func makeUIViewController(context: UIViewControllerRepresentableContext<Integrated_PSPDFViewController>) -> PSPDFViewController {
//        return PSPDFViewController(document: currentPDF)
//    }
//    
//    func updateUIViewController(_ uiViewController: PSPDFViewController, context: UIViewControllerRepresentableContext<Integrated_PSPDFViewController>) {
//        uiViewController.document = currentPDF
//    }
//    
//    /*class Coordinator: NSObject {
//    var control: PSPDFIntegratedController
//    
//    init(_ control: PSPDFIntegratedController) {
//    self.control = control
//    }
//    
//    @objc func updateCurrentPDF(sender: PSPDFDocument) {
//    control.currentPDF = sender
//    }
//    }*/
//}
