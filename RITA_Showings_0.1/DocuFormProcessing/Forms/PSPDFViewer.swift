//
//  PSPDFViewer.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/5/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import SwiftUI
import PSPDFKit

struct PSPDFViewer : View {
	var currentPDF : PSPDFDocument?
	
	init(path : String) {
		self.currentPDF = PSPDFDocument( url: getFileUrl(pathToFile: path) )
	}
	
	var body: some View {
		PSPDFKitView(document: currentPDF, configuration: PSPDFConfiguration { $0.pageTransition = .scrollPerSpread })
		
	}
}

struct PSPDFViewerAtLocation : View {
	var currentPDF : PSPDFDocument
	
	init(doc : PSPDFDocument) {
		self.currentPDF = doc
	}
	
	var body: some View {
		PSPDFKitView(document: currentPDF, configuration: PSPDFConfiguration { $0.pageTransition = .scrollPerSpread })
		
	}
}

#if DEBUG
struct PSPDFViewerAtLocation_Previews : PreviewProvider {
    static var previews: some View {
        PSPDFViewerAtLocation(doc: {
            PSPDFDocument( url: getFileUrl(pathToFile: pathToFormDemo) )
        }())
    }
}
#endif

#if DEBUG
struct PSPDFViewer_Previews : PreviewProvider {
    static var previews: some View {
        PSPDFViewer(path: pathToPDFDemo)
    }
}
#endif

@available(iOS 13.0, *)
struct PSPDFKitView: UIViewControllerRepresentable {
    let document: PSPDFDocument
    let configuration: PSPDFConfiguration?

    func makeUIViewController(context: UIViewControllerRepresentableContext<PSPDFKitView>) -> UINavigationController {
        // Create the PSPDFViewController
        let pdfController = PSPDFViewController(document: document, configuration: configuration)
        return UINavigationController(rootViewController: pdfController)
    }

    func updateUIViewController(_ uiViewController: UINavigationController, context: UIViewControllerRepresentableContext<PSPDFKitView>) {
        // Update the view controller.
    }
}

//struct Integrated_PSPDFViewController : UIViewControllerRepresentable {
//	var currentPDF : PSPDFDocument?
//
//	/*func makeCoordinator() -> Coordinator {
//	Coordinator(self)
//	}*/
//
//	func makeUIViewController(context: UIViewControllerRepresentableContext<Integrated_PSPDFViewController>) -> PSPDFViewController {
//		return PSPDFViewController(document: currentPDF)
//	}
//
//	func updateUIViewController(_ uiViewController: PSPDFViewController, context: UIViewControllerRepresentableContext<Integrated_PSPDFViewController>) {
//		uiViewController.document = currentPDF
//	}
//
//	/*class Coordinator: NSObject {
//	var control: PSPDFIntegratedController
//
//	init(_ control: PSPDFIntegratedController) {
//	self.control = control
//	}
//
//	@objc func updateCurrentPDF(sender: PSPDFDocument) {
//	control.currentPDF = sender
//	}
//	}*/
//}

//func getFileUrl(pathToFile: String) -> URL {
//	let url = Bundle.main.url(forResource: "\(pathToFile)", withExtension: "pdf")
//	return url!
//}
