//
//  FieldCatagorizationView.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/8/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import SwiftUI
import PSPDFKit

//	Intended to be a View that will the user visually rearrange form fields
//	into horizontal catagory rows by dragging and dropoing from one catagory
//	view to the other
struct FieldCatagorizationView : View {
    @EnvironmentObject var formData: FormTemplateData
    
	// From "Composing Complex Interfaces" tutorial on Apple Dev
	// Categorizes the items based on catagory names
	var categories: [String : [FormField]] {
		.init(
			grouping: self.items.values,
			by: { $0.category.name }
		)
	}
	
	var featured: [FormField] {
		items.values.filter { $0.isFeatured }
	}
    @State var category : FieldCategory
	@State var items: [String: FormField]
    
    @State var showingProfile = false
    
    var profileButton: some View {
        Button(action: { self.showingProfile.toggle() }) {
            Image(systemName: "person.crop.circle")
                .imageScale(.large)
                .accessibility(label: Text("User Profile"))
                .padding()
        }
    }
    
	var body: some View {
		NavigationView {
			List {
				FeaturedFields(fields: featured)
					.scaledToFit()
					.frame(height: CGFloat(200))
					.clipped()
					.listRowInsets(EdgeInsets())
				
                ForEach(categories.keys.sorted(), id: \.self) { key in
					Text("Test")
				}
				.listRowInsets(EdgeInsets())
				
                NavigationLink(destination: FieldsFoundList(formControl: $formControl)) {
					Text("See All")
				}
			}
			.navigationBarTitle(Text("Featured"))
            .navigationBarItems(trailing: profileButton)
            .sheet(isPresented: $showingProfile) {
                    Text("User Profile")
            }
		}
	}
}

#if DEBUG
struct FieldCatagorizationView_Previews : PreviewProvider {
	static var previews: some View {
		FieldCatagorizationView(category: FieldCategory.alwaysInput,items:
            debugParser.Fields_Extracted, formControl: FormTemplateControl(fileParser: debugParser))
	}
}
#endif

struct CategoryRow: View {
	var categoryName: String
	@Binding var items: [FormField]
	
	var body: some View {
		VStack(alignment: HorizontalAlignment.leading) {
			Text(self.categoryName)
				.font(.headline)
				.padding(.leading, 15)
				.padding(.top, 5)
			
			ScrollView {
				HStack(alignment: .top, spacing: 0) {
					ForEach(self.$items) { field in
						NavigationLink(
							destination: FieldDetailView(
							field: field
						)) {
//							CategoryItem(field: field)
							Text("")
						}
					}
				}
			}
			.frame(height: 185)
		}
	}
}

#if DEBUG
struct CategoryRow_Previews : PreviewProvider {
	static var previews: some View {
		CategoryRow(
			categoryName: debugParser.Fields_Extracted.values.first?.category.name ?? "Failed to Get Category Name",
			items: Array(debugParser.Fields_Extracted.values.prefix(4))
		)
	}
}
#endif

struct CategoryItem: View {
	var field: FormField
	var body: some View {
		VStack(alignment: .leading) {
			Image(field.name).resizable()
				.scaledToFit()
				.frame(height: 155)
				.clipped()
				.cornerRadius(5)
			Text(field.name)
                .foregroundColor(.primary)
				.font(.caption)
		}
		.padding(.leading, 15)
	}
}


struct FeaturedFields: View {
	var fields: [FormField]
	var body: some View {
		Image(fields[0].name).resizable()
	}
}
