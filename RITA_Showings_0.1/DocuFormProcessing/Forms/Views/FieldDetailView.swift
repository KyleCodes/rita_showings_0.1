//
//  FieldDetailView.swift
//  
//
//  Created by Kyle Teixeira on 7/5/19.
//

import SwiftUI
import Combine
import PSPDFKit

struct FieldDetailView : View {
    @EnvironmentObject var viewModel : FormTemplate_ViewModel
	@State var field : FormField
    
    @State private var showFieldAtLocation = false
        
    var seeAtDocLocationPresentButton: some View {
        HStack {
            Button(action: { self.showFieldAtLocation.toggle() }) {
                Text("View on Document at Location")
            }
        }
    }
    
    var body: some View {
		NavigationView {
			VStack {
				Form {
					Text("Raw String - \(field.wordPS?.stringValue ?? "")")
					Section {
                        Toggle(isOn: $field.required) {
                            Text("Is Required?")
                        }
                        Toggle(isOn: $field.doesChange) {
                            Text("Does Change?")
                        }
					}
					Section(header: Text("Default Value")) {
						HStack {
                            TextField("Field Value", text: $field.defaultText)
						}
						
						VStack(alignment: .leading) {
							Text("Fillable Type")
							HStack(alignment: .center) {
								Spacer()
                                Picker(selection: $field.category, label: Text("Field Category"), content: {
									ForEach(FieldCategory.allCases) { category in
										Text(category.name).tag(category)
									}.allowsTightening(true)
                                    }).pickerStyle(SegmentedPickerStyle())
								Spacer()
							}
						}
						HStack {
							Text("Fill From") // User/client data to fill from
							Spacer()
							Text("FillFromField")
						}
					}
				}
                self.seeAtDocLocationPresentButton
			}.padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5))
			.navigationBarTitle(Text("\(field.name)"))
            .sheet(isPresented: $showFieldAtLocation) {
                self.seeAtDocLocationPresentButton
            }
		}
    }
}

#if DEBUG
struct FieldDetailView_Previews : PreviewProvider {
    static var previews: some View {
		// PDF Parser
        //FieldDetailView(field: debugParser.formFields_Extracted.popFirst()?.value ?? FormField(name: "testField", maxLength: 10))
		
		//Form Parser
        FieldDetailView(field: debugParser.Fields_Extracted.popFirst()!.value )
    }
}
#endif
