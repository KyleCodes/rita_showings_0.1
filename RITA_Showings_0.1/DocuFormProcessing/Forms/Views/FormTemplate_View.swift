//
//  FormTemplate_View.swift
//  RITA-Showings_0.1
//
//  Created by Kyle Teixeira on 6/24/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import SwiftUI
import Combine
import PSPDFKit

struct FormTemplate_View : View {
    @EnvironmentObject var viewModel : FormTemplate_ViewModel
	
    var body: some View {
        NavigationView {
            FieldsFoundList()
                .navigationBarTitle("New Form Template")
        }
    }
}


struct FieldsFoundList : View {
    @EnvironmentObject var viewModel : FormTemplate_ViewModel
    @State var searchText: String = String()
    @State var isSearching: Bool = false
    
    var body: some View {
        List {
            SearchBar_Custom(searchText: $searchText, isSearching: $isSearching)
            ForEach(viewModel.groupedFormFields.indices, id: \.self) { g in
                Group {
                    Section(header: Text("\(self.viewModel.groupedFormFields[g].groupName)") ){
                        ForEach(self.viewModel.groupedFormFields[g].fields) { currentfield in
                            if self.isSearching == false || currentfield.name_lower.contains(self.searchText.lowercased()) {
                                NavigationLink(destination:
                                    FieldDetailView(field: currentfield)
                                ) {
                                    FieldRowItem(field: currentfield)
                                }
                            }
                        }
                    }
                }
                
            }
        }
        .padding(10)
        .listRowInsets(EdgeInsets())
        .navigationBarItems(trailing:
             Text("Fields Found: \(viewModel.fieldBoxes.count)")
            .font(.subheadline)
            .multilineTextAlignment(.center))
    }
}

#if DEBUG
struct FormTemplateView_Previews : PreviewProvider {
	static var previews: some View {
		FormTemplate_View()
            .environmentObject(FormTemplate_ViewModel(fileParser: debugParser))
	}
}
#endif

//#if DEBUG
//struct FieldRowItem_Previews : PreviewProvider {
//	static var previews: some View {
//		FieldRowItem(field: debugParser.Fields_Extracted.popFirst()?.value ?? FormField(name: "testField", maxLength: 10))
//			.previewLayout(.fixed(width: 400, height: 100))
//	}
//
//}
//#endif
struct Builder<Content> : View where Content : View {
    var content: Content

    @inlinable public init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }

    var body : some View {
        VStack {
            self.content
        }
    }
}

struct FieldRowItem : View {
    var field : FormField
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(field.name)
       .font(.headline)
            HStack(alignment: .top) {
                Text("String: \(field.formField?.exportValue as! String? ?? "Err reading String")")
                    .font(.footnote)
                    .fontWeight(.medium)
                Spacer()
                 Text("Type: \(field.category.name)")
                    .font(.caption)
                    .foregroundColor(.gray)
                    .multilineTextAlignment(.trailing)
            }
        }
    }
}

struct SearchBar_Custom : View {
    @Binding var searchText: String
    @Binding var isSearching: Bool
    
    var body: some View {
        VStack {
            if isSearching {
                Text("Search will now Refresh as you Type")
            }
            HStack {
                Image(systemName: "magnifyingglass")
                    .foregroundColor(.secondary)
                TextField("Search", text: self.$searchText, onEditingChanged: { _ in
                       // self.formData.isSearching = false // once typing begins again stops searching
                }, onCommit: {
                    self.isSearching = true  // on return searches for term
                }).autocapitalization(.none)
                Button(action: {
                    self.searchText = String()
                    self.isSearching = false
                }) {
                Image(systemName: "xmark.circle.fill")
                    .foregroundColor(.secondary)
                    .opacity(self.searchText == "" ? 0 : 1)
                }
            }.padding(.horizontal)
        }
    }
}

