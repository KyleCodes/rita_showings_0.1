//
//  OfferTerms_View.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/30/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import SwiftUI
import Combine
import Foundation
import MapKit

struct OfferTerms_View: View {
    @EnvironmentObject var viewModel: OfferTerms_ViewModel
    @ObservedObject var clients = g_clients
    
    let title = "Offer Terms"
    @State var manualAddress = Address()
    @State var address: String = ""
    @State var searching_address: Bool = false
    @State private var searchQuery: String = ""
    @State private var showAddressSelector = false
    @State private var pressedConfirm = false
    
    var sheet_AddressSelector: some View {
        VStack {
            VStack {
                MapView(locationManager: $viewModel.locationManager)
                    .edgesIgnoringSafeArea(.vertical)
            }
            HStack {
                TextField("Subject Property Address", text: $address, onCommit: {
                    self.searching_address = true
                })
                    .autocapitalization(.none)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal)
            }
            Divider()
            if searching_address {
                List(self.viewModel.searchResults_Address, id: \.self) { item in
                    Text("\(item.title!)\n\(item.subtitle!)\n\(item.countryCode!)\n\(item.name!)\n\(item.description)")
                }
            }
            Spacer()
            VStack {
                Divider()
                Button(action: { self.showAddressSelector.toggle()
                    self.searching_address = false
                }) {
                    Text("Done")
                }
            }
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("Subject Property")) {
                    Group {
                        Button(action: { self.showAddressSelector.toggle() }) {
                            Text("Select Subject Property")
                        }
                        TextField("Address 1", text: $manualAddress.line_1)
                            .autocapitalization(.words)
                            .multilineTextAlignment(.leading)
                            .padding(.horizontal)
                        TextField("Address 2", text: $manualAddress.line_2)
                            .autocapitalization(.words)
                            .multilineTextAlignment(.leading)
                            .padding(.horizontal)
                        HStack {
                            TextField("City", text: $manualAddress.City)
                            .autocapitalization(.words)
                            .multilineTextAlignment(.leading)
                            .padding(.horizontal)
                            TextField("State", text: $manualAddress.State)
                            .autocapitalization(.words)
                            .multilineTextAlignment(.leading)
                            .padding(.horizontal)
                            TextField("Zip", text: $manualAddress.Zip)
                            .autocapitalization(.words)
                            .multilineTextAlignment(.leading)
                            .padding(.horizontal)
                        }
                    }
                }
                Section(header: Text("Primary Client"), footer: Text("End of Search")) {
                    Group {
                        SearchBar(searchQuery: $searchQuery)
                        List {
                            ForEach(clients.list.values.filter {
                                $0.legalFullName.lowercased().contains(self.searchQuery.lowercased()) || searchQuery == "" || $0.email_address.lowercased().contains(self.searchQuery.lowercased()) || $0.mobile_phone_number.lowercased().contains(self.searchQuery.lowercased()) || $0.additional_phone_numbers[0].lowercased().contains(self.searchQuery.lowercased())
                            }, id: \.self) { item in
                                ClientListRow(item: item)
                            }
                        }
                    }
                }
            }
            Group {
                Button(action: {
                    self.pressedConfirm = true
                }) {
                    Text("Confirm")
                    .bold()
                }
//                .alert(isPresented:  self.pressedConfirm && self.$selectedClient.isEmpty ) {
//                    Alert(title: "You have not selected a Primary Client, are you sure you want to continue?", message: "You will have to start over to return here", primaryButton: .default(Text("Continue"), action: {
//                        // Go to next view without Client Selected
//                    }), secondaryButton: .cancel())
//                }
    //            .alert(isPresented: pressedConfirm && selectedClient.count > 1) {
    //                Alert(title: "You have selected more than 1 client.", message: "Any clients grouped to these clients will also be used", primaryButton: .default(Text("Continue"), action: {
    //                    // Go to next view with multiple Clients Selected
    //                }), secondaryButton: .cancel())
    //            }
            }
        }
        .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5))
        .navigationBarTitle(Text(title))
        .sheet(isPresented: self.$showAddressSelector) {
            self.sheet_AddressSelector
        }
        }
    }
}

struct OfferTerms_View_Previews: PreviewProvider {
    static var previews: some View {
        OfferTerms_View().environmentObject(OfferTerms_ViewModel())
    }
}

struct ClientListRow: View {
    @EnvironmentObject var viewModel: OfferTerms_ViewModel
    @ObservedObject var item: Client
    var body: some View {
        Group {
            Button(action: {
                self.item.isSelected.toggle()
                if self.item.isSelected {
                    self.viewModel.selectedClient.append(self.item)
                } else {
                    self.viewModel.selectedClient.remove(at: self.viewModel.selectedClient.firstIndex(of: self.item)!)
                }
            }) {
                HStack {
                    Text("\(item.firstName) --")
                        .foregroundColor(.primary)
                    Text("\(item.legalFullName)")
                        .foregroundColor(.primary)
                    Spacer()
                    if self.item.isSelected {
                        Image(systemName: "checkmark.circle.fill")
                    } else {
                        Image(systemName: "checkmark.circle")
                    }
                }
            }
        }
    }
}

struct SearchBar: View {
    @Binding var searchQuery: String
    var body: some View {
        Group {
            HStack {
                Image(systemName: "magnifyingglass")
                    .padding(.leading, CGFloat(10.0))
                TextField("Search", text: $searchQuery, onEditingChanged: { active in
                    print("Editing changed: \(active)")
                }, onCommit: {
                    print("Commited: \(self.searchQuery)")
                })
                    .padding(.vertical, CGFloat(4.0))
                    .padding(.trailing, CGFloat(10.0))
            }
            .overlay(
                RoundedRectangle(cornerRadius: 5.0)
                    .stroke(Color.secondary, lineWidth: 1.0)
            )
                .padding()
        }
    }
}

struct SearchBar_UIViewRep: UIViewRepresentable {

    @Binding var text: String

    class Coordinator: NSObject, UISearchBarDelegate {

        @Binding var text: String

        init(text: Binding<String>) {
            _text = text
        }

        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            text = searchText
        }
    }

    func makeCoordinator() -> SearchBar_UIViewRep.Coordinator {
        return Coordinator(text: $text)
    }

    func makeUIView(context: UIViewRepresentableContext<SearchBar_UIViewRep>) -> UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.delegate = context.coordinator
        return searchBar
    }

    func updateUIView(_ uiView: UISearchBar,
                      context: UIViewRepresentableContext<SearchBar_UIViewRep>) {
        uiView.text = text
    }
}
