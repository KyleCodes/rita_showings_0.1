//
//  OfferTerms_ViewModel.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/30/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import PSPDFKit
import MapKit

class OfferTerms_ViewModel: ObservableObject, Identifiable {
    let id = UUID()
    var offerTerms: OfferTerms
    @Published var locationManager: LocationManager
    @Published var selectedClient: [Client] = []
    
    func imageName(selected: Bool) -> String {
        var imageName: String
        if selected {
            imageName = "checkmark.circle.fill"
        } else {
            imageName = "checkmark.circle"
        }
        return imageName
    }
    
    var subjectProperty: Property {
        get {
            offerTerms.subjectProperty
        }
        set {
            offerTerms.subjectProperty = newValue
        }
    }
    
    init() {
        self.locationManager = LocationManager()
        self.offerTerms = OfferTerms()
        load()
    }
    
    func load() {
        
    }
    
    @Published var searchResults_Address: [MKPlacemark] = []
    
    func searchAddresses(searchText: String) {
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = searchText
        searchRequest.region = locationManager.region
        let search = MKLocalSearch(request: searchRequest)
        search.start { response, error in
            guard let resp = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }
            for item in resp.mapItems {
                self.searchResults_Address.append(item.placemark)
                print("\(String(describing: item.placemark.title))\n\(String(describing: item.placemark.subtitle))\n\(String(describing: item.placemark.countryCode))\n\(String(describing: item.name))\n\(item.description)")
            }
        }
    }
}
