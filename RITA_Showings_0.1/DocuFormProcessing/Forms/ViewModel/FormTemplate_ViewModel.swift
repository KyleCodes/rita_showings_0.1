//
//  FormTemplate_ViewModel.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 8/26/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import PSPDFKit

class FormTemplate_ViewModel: ObservableObject, Identifiable {
    let id = UUID()
    private let formTemplate: FormTemplate
    var templateName: String = "Test Template"
    
    
    @Published var fieldBoxes : [FormField]
    
    @Published var groupedFormFields : [groupedListData] = [
            groupedListData(groupName: "Text"),
            groupedListData(groupName: "Checkbox"),
            groupedListData(groupName: "Radio Buttons"),
            groupedListData(groupName: "Combo"),
            groupedListData(groupName: "List"),
            groupedListData(groupName: "Push Button"),
            groupedListData(groupName: "Unknown")
    ]
   
    
    init(fileParser: FormFileParser) {
        self.formTemplate = FormTemplate(fileParser: fileParser, name: templateName)
        self.fieldBoxes = []
        loadAndGroupFields(fileParser: fileParser)
    }
    
    func loadAndGroupFields(fileParser: FormFileParser) {
        for fields in fileParser.Fields_Extracted {
            self.fieldBoxes.append(fields.value)
            
            switch fields.value.type {
                case .text:
                    groupedFormFields[0].fields.append(fields.value)
                case .checkBox:
                    groupedFormFields[1].fields.append(fields.value)
                case .radioButton:
                    groupedFormFields[2].fields.append(fields.value)
                case .comboBox:
                    groupedFormFields[3].fields.append(fields.value)
                case .listBox:
                    groupedFormFields[4].fields.append(fields.value)
                case .pushButton:
                    groupedFormFields[5].fields.append(fields.value)
                case .unknown:
                    groupedFormFields[6].fields.append(fields.value)
                case .signature:
                    break
                case nil:
                    break
            }
        }
    }
    
    class groupedListData : Identifiable, Hashable {
        static func == (lhs: FormTemplate_ViewModel.groupedListData, rhs: FormTemplate_ViewModel.groupedListData) -> Bool {
            return lhs.id == rhs.id && lhs.groupName == rhs.groupName
        }
        func hash(into hasher: inout Hasher) {
             hasher.combine(id)
             hasher.combine(groupName)
         }
        
        let id = UUID()
        var groupName : String = ""
        var fields : [FormField] = []
        
        init(groupName : String) {
            self.groupName = groupName
        }
        init(groupName : String, fields : [FormField]) {
            self.groupName = groupName
            self.fields = fields
        }
    }
}
