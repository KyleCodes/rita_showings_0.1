//
//  OfferTerms.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/28/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import SwiftUI
import PSPDFKit

class OfferTerms: Identifiable {
    let id = UUID()
    
    var defaults: OfferDefaults = OfferDefaults()
    var primaryClient: UUID? {
        didSet {
            //  Make sure new UUID is in Client List
            if !g_clients.list.contains(where: { it in
                if it.key == it.value.id {
                    return true
                }else {
                    return false
                }
            }) {
                // Sets back to oldValue if not in client list
                primaryClient = oldValue
            }
        }
    }
    var sellers: [Person.ID: Person] = [:]
    var subjectProperty = Property()
    
    var offerPrice = Money()
    private var p_downPaymentPercentage = Double()
    var downPaymentPercentage: Double {
        get { return p_downPaymentPercentage }
        set(new) {
            if (0...1).contains(new) {
                p_downPaymentPercentage = new
            } else {
                p_downPaymentPercentage = new<0 ? 0 : 1
            }
        }
    }
    lazy var downPaymentAmt: Money = {
        let dollarAmt = self.offerPrice.asDouble() * self.downPaymentPercentage
        return Money(dollars: dollarAmt)
    }()
    lazy var financingTotalAmt: Money = {
        let dollarAmt = self.offerPrice.asDouble() - self.downPaymentAmt.asDouble()
        return Money(dollars: dollarAmt)
    }()
    //  Defaults to 1% of Offer Price
    lazy var earnestAmt: Money = {
        let dollarAmt = self.offerPrice.asDouble() * 0.01
        return Money(dollars: dollarAmt)
    }()
    var additionalEarnestAmt: Money?
    lazy var licenseHolderDisclosure: String = {
        // Add disclosues specific to Client
        return ""
    }()
    
    // Computed Properties
    var fullAddress_asString_singleLine: String {
        get {
            var add = "\(subjectProperty.address.line_1)"
            if subjectProperty.address.line_2 != "" {
                add += ", \(subjectProperty.address.line_2)"
            }
            add += ", \(subjectProperty.address.City), \(subjectProperty.address.State) \(subjectProperty.address.Zip)"
            return add
        }
    }
    var fullAddress_asString_doubleLine: String {
        get {
            var add = "\(subjectProperty.address.line_1)"
            if subjectProperty.address.line_2 != "" {
                add += ", \(subjectProperty.address.line_2)"
            }
            add += "\n\(subjectProperty.address.City), \(subjectProperty.address.State) \(subjectProperty.address.Zip)"
            return add
        }
    }
    
    //  Must put offer price, dp percent,
    init() {
        
    }
}

class OfferDefaults: Identifiable {
    let id = UUID()
    
    //  Set default fields that generally don't change
    //  from offer to offer
    var prohibitedActivities: String = ""
    var inHOA: Bool = true
    var sellerDisclosure: receiptOfSellerDisclosure = .hasNotReceived
    lazy var daysToFurnishSellerDisclosure: Int = 3
    var acceptsProperty_AsIs = true
    lazy var repairs_treaments: String = ""
    
    //  Should hold the UUID's of all the addenda that need to be checked
    //  off.  This would generally include Third Party Financing and more for a Buyer
    var attachedAddenda: [UUID] = []
    
    var sellingAgent_commissionPercentage: Double = 3.0
    
    struct dates {
        var closing: Date
        var signed: [Date]
        var financingApprovalDueBy: Date?
        var optionPeriodEnds: Date?
        private var execution_readOnly: Date?
        var execution: Date {
            get {
                return execution_readOnly!
            }
            set {
                //  Only set execution date once
                if execution_readOnly == nil {
                    execution_readOnly = newValue
                }
            }
        }
    }
    
    func noticeSection(client: Client.ID) -> ContactInfoSection {
        return ContactInfoSection(primaryClient: client)
    }
    func attorneySection(attorney: Person.ID) -> ContactInfoSection {
        return ContactInfoSection(primaryClient: attorney)
    }
    struct ContactInfoSection {
        var namesOfParties = String()
        var extraLine: String?
        var phoneNums: [String] = []
        var emails: [String] = []
        var faxNum: String?
        
        init(primaryClient: Person.ID) {
            guard let currentClient = g_clients.list[primaryClient] else {
                self.namesOfParties = "*Client not found*"
                return
            }
            self.namesOfParties = {
                var str = currentClient.legalFullName
                self.emails.append(currentClient.email_address)
                self.phoneNums.append(currentClient.mobile_phone_number)
                self.phoneNums.append(contentsOf: currentClient.additional_phone_numbers)
                if currentClient.isGrouped() {
                    for g in currentClient.groupedWith! {
                        let curr = g_clients.list[g]!
                        str += ", \(String(describing: curr.legalFullName))"
                        self.emails.append(curr.email_address)
                        self.phoneNums.append(curr.mobile_phone_number)
                        self.phoneNums.append(contentsOf: curr.additional_phone_numbers)
                    }
                }
                return str
            }()
        }
    }
    
    //  Offer fields that can be changed to incentivize offer
    //  such as Buyer paying for Title, Survey, etc.
    struct incentives {
        var sellerPaysTitlePolicy = true
        var exceptionsInTitlePolicy: titlePolicyExceptions = .willNOTBeAmended
        var survey_howToObtain: acceptanceOfSurvey = .sellerShallFurnish_elseSelleerPays
        var survey_daysToObtain: Int = 3
        var closingCostsPaidBySeller_forBuyer: Money = Money(cents: 0)
        var allowance_forServiceContracts: Money = Money(cents: 0)
        var possession: PossessionType = .closingAndFunding
        var specialProvision: String?
        var option_fee: Money = Money(dollars: 100.00)
        var option_days: Int = 7
        var option_creditedToSalesPrice: Bool = true
        
        lazy var additional: [String: String] = [:]
    }
    enum PossessionType {
        case closingAndFunding
        case temporaryLease
        case other
    }
    enum titlePolicyExceptions {
        case willNOTBeAmended
        case willBeAmended_BuyerExpense
        case willBeAmended_SellerExpense
    }
    enum acceptanceOfSurvey {
        case sellerShallFurnish_elseBuyerPays
        case sellerShallFurnish_elseSelleerPays
        case buyerToObtainNewSurvey
        case SellerToObtainNewSurvey
    }
    enum receiptOfSellerDisclosure {
        case hasBeenReceived
        case hasNotReceived
        case notRequired
    }
}
