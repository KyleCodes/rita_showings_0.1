//
//  FormTemplate.swift
//  RITA-Showings_0.1
//
//  Created by Kyle Teixeira on 6/25/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import PSPDFKit

//  This is the model for the Form
class FormTemplate : ObservableObject, Hashable, Identifiable {
    static func == (lhs: FormTemplate, rhs: FormTemplate) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.fileName == rhs.fileName && lhs.filePath == rhs.filePath
    }
    func hash(into hasher: inout Hasher) {
         hasher.combine(id)
         hasher.combine(name)
         hasher.combine(filePath)
         hasher.combine(dateCreated)
         hasher.combine(lastModified)
     }
	let id = UUID()
    var fileParser: FormFileParser
    var formNumber: Int
    
	var name: String
	fileprivate var fileName: String
	fileprivate var filePath: String?
	fileprivate var fileURL: URL?
	fileprivate let dateCreated: Date
	fileprivate let createdBy: UUID
	fileprivate var owner: UUID
	fileprivate var lastModified: Date
    
    private static var formCount: Int = 0
    
    init(fileParser: FormFileParser, name: String) {
        self.fileParser = fileParser
        FormTemplate.formCount += 1
        self.formNumber = FormTemplate.formCount
        self.fileURL = fileParser.currentDoc.fileURL
        self.fileName = fileParser.currentDoc.fileName ?? "FormTemplate_\(FormTemplate.formCount)_(fileName not found)"
		self.name = name
		self.dateCreated = Date()
		self.lastModified = self.dateCreated
		self.owner = currentUser.id
		self.createdBy = currentUser.id
		//self.fileURL = getFileUrl(pathToFile: path)
		/*
			Need to finish initiallizing Form from PDF.
			Not itended to be the Template Model, just the file's info holder.
		*/
	}
	
	func getFileUrl(pathToFile: String) -> URL {
		let path = Bundle.main.path(forResource: "\(pathToFile)", ofType: "pdf")
		let url = URL(fileURLWithPath: path! )
		return url
	}
}
