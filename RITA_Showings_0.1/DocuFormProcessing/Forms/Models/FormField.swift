//
//  FormField.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/5/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import SwiftUI
import PSPDFKit

//  This is the model for a Form Field
class FormField : Identifiable, Hashable {
    static func == (lhs: FormField, rhs: FormField) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
   func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
    }
    
	var id = UUID()
	var name = String()
    var name_lower = String()
	var defaultValue : Any?
	var defaultText = ""
	var maxLength : Int = 0
	var category = FieldCategory.alwaysInput
    var type: PSPDFFormFieldType?
	var required = true
	var doesChange = true
	var fillable = false
	var ignore = false
	var wordPS : PSPDFWord?
	var formField : PSPDFFormField?
	var formElement : PSPDFFormElement?
	var isAnnotation = false
	var isFeatured = false
    var documentOrder = -1
    
	// Form parsed init for AcroForm fields found in document
	init(formField : PSPDFFormField) {
		self.formField = formField
		self.name = formField.name ?? formField.fullyQualifiedName ?? "Unnammed Field_\(id)"
        self.name_lower = self.name.lowercased()
		self.type = formField.type
	}
	
	// Form parsed init for AcroForm Annotations found in document
	init(formElement : PSPDFFormElement) {
		self.formElement = formElement
		self.name = formElement.name ?? formElement.fieldName ?? formElement.fullyQualifiedFieldName ?? "Unnammed Element_\(id)"
        self.name_lower = self.name.lowercased()
		self.isAnnotation = true
        self.type = formField?.type
	}
	
	// PDF Parsed init for found words as PSPDFWord
	init(name : String, maxLength : Int, wordPS : PSPDFWord) {
		self.name = name
        self.name_lower = name.lowercased()
		self.maxLength = maxLength
		self.wordPS = wordPS
	}
	
	#if DEBUG
	init(name : String, maxLength : Int) {
		self.name = name
        self.name_lower = name.lowercased()
		self.maxLength = maxLength
	}
	#endif
}

enum FieldCategory: Int, CaseIterable, Identifiable, Hashable {
	case alwaysInput
	case fillable
	case defaultValue

	var id: UUID {
		return UUID()
	}
	var name: String {
		switch (self) {
		case .alwaysInput:
			return "Always Input"
		case .fillable:
			return "Fillable"
		case .defaultValue:
			return "Default Value"
		}
	}
	
	enum fillableType: Int, CaseIterable, Identifiable, Hashable {
		case agentInfo
		case clientInfo
		case retsFeed
		case taxFeed
		
		var id: UUID {
			return UUID()
		}
		
		var name: String {
			switch (self) {
			case .agentInfo:
				return "Agent Info"
			case .clientInfo:
				return "Client"
			case .retsFeed:
				return "RETS Feed"
			case .taxFeed:
				return "Tax Fee"
			}
		}
	}
}

extension UIColor {
	func imageWithColor(width: Int, height: Int) -> UIImage {
		let size = CGSize(width: width, height: height)
		return UIGraphicsImageRenderer(size: size).image { rendererContext in
			self.setFill()
			rendererContext.fill(CGRect(origin: .zero, size: size))
		}
	}
}
