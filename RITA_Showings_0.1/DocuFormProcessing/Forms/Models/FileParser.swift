//
//  FileParser.swift
//  RITA-Showings_0.1
//
//  Created by Kyle Teixeira on 7/1/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import SwiftUI
import PSPDFKit

public class FormFileParser {
	var currentDoc : PSPDFDocument
	var formParser : PSPDFFormParser?
	var Fields_Extracted : [String: FormField] = [:]
	
	init(pdf: PSPDFDocument) {
		currentDoc = pdf
		extractFormFields(currentDoc: currentDoc)
	}
	
	//  Will extract all fields that are in the format
	//  starting and ending with "(([...]))"
	func extractFormFields(currentDoc : PSPDFDocument) {
		// https://pspdfkit.com/guides/ios/current/features/text-extraction/
		
		// Working on form parsing
		formParser = currentDoc.formParser!
		for found in (formParser?.formFields)! {
            Fields_Extracted[found.fullyQualifiedName ?? "\(found.hashValue)"] = FormField(formField: found)
		}
	}
}

class PDFFileParser {
	var currentDoc : PSPDFDocument
	var Fields_Extracted : [String: FormField] = [:]
	
	init(pdf: PSPDFDocument) {
		currentDoc = pdf
		extractPDFFields(currentDoc: currentDoc)
	}
	
	//  Will extract all fields that are in the format
	//  starting and ending with "(([...]))"
	func extractPDFFields(currentDoc : PSPDFDocument) {
		// https://pspdfkit.com/guides/ios/current/features/text-extraction/
		
		//  parse document for words
		let textParser = currentDoc.textParserForPage(at: 0)!
		// Fields where the field name is led with "(([" & ended with "]))"
		//let regexp = ".*\\(\\(\\[.*\\]\\)\\).*"
		for word in textParser.words {
			if word.stringValue.contains("(([") {
				let str = word.stringValue
				var startIndex = str.firstIndex(of: "[") ?? str.startIndex
				startIndex = str.index(after: startIndex) //offset by 1 char
				let endIndex = str.firstIndex(of: "]") ?? str.endIndex
				let name = String(str[startIndex..<endIndex])
				let ffield: FormField = FormField(name: name, maxLength: str.count, wordPS: word)
				Fields_Extracted[name] = ffield
			}
		}
	}
}
