//
//  FormLibrary.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/24/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import PSPDFKit

class LibraryExample {

    func indexDocuments() {
        guard let library = PSPDFKit.sharedInstance.library else { return }
        let filesURL = Bundle.main.bundleURL.appendingPathComponent("FormLibrary")

        let dataSource = PSPDFLibraryFileSystemDataSource(library: library, documentsDirectoryURL: filesURL)
        library.dataSource = dataSource
        library.updateIndex()

        // Indexing is async, we could use notifications to track the state,
        // but for this example it's easy enought to just delay this for a second.
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            library.documentUIDs(matching: "pdf", options: nil, completionHandler: { searchString, resultSet -> Void in
                print("For \(searchString) found \(resultSet)")
            }, previewTextHandler: nil)
        }
    }
}
