//
//  ContentView.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/5/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    @EnvironmentObject var viewModel_forms: FormTemplate_ViewModel
    @EnvironmentObject var viewModel_offerTerms: OfferTerms_ViewModel
    var body: some View {
		//FormTemplate(fileParser: debugParser)
        HStack {
            //AgentProfileDetail()
            //FormTemplate_View()
            OfferTerms_View()
        }
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
