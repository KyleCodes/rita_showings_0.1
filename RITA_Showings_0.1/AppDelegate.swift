//
//  AppDelegate.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/5/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import PSPDFKit
import UIKit
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		
        //  Expires 2019-12-01 //
        PSPDFKitGlobal.setLicenseKey("F6JCwVmkGp/m5VIDP6WTwH7uVoZFzXDkyXPOBn8d1A+T7hMiSmO37c7Mz3ZDalOJNBazQMjGBLDs+pDsMK0Fi3n4OXs5TC99zAxmPtjUksVzml7t0Bt8BItR7Dl+CZhReIR3cLrs0IB40pkt2L7IH4KZ02vkixnaexlujiPmgu0wPJqn06t+QiVFVmJZUbkVFAATCOY0ODEK4gLvl4Fe+V5nyvaJmhxe+K+Ev/pvQ5SV0azg5cyaQ6SgIcX3JCLgLhGqHYiLVG+QHSiGN6USr6y8/eElzocdQfl8qSl4kr1leXif04MtJ8le6iRpm2BQ+f4bnnAsXxcSaWAOuZ6SOl/y6uBM551a7Id1aaR+Elgvo8NheIYg0x7ghspCZs7JEBh3EC01NlFv9/syws4YMcF0qSGSdHJpA3nGhwwcwKMlYYT4Q1Dpgt2iiifJuGXJ")
       
        //  Google Places/Maps SDK API Key //
        GMSPlacesClient.provideAPIKey("AIzaSyB5TwUnXFLhl_UzQ45VHKPepwWwqPDu_sQ")
        
		return true
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}


}

