//
//  Global_TestingContainers.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/29/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import PSPDFKit

//  Empty Dictionary of Client's
//  -- initially for testing; subsititue with CloudKit
final class ClientList: ObservableObject {
    @Published var list: [Client.ID: Client] = [:]
}
var g_clients = ClientList()

#if DEBUG
let pathToPDFDemo : String = "PurchaseContract"
let pathToFormDemo = "PurchaseContract-Form"

// PDF Parser
//let debugParser = PDFFileParser(pdf: PSPDFDocument(url: getFileUrl(pathToFile: pathToPDFDemo) ) )

// Form Parser
let debugParser = FormFileParser(pdf: PSPDFDocument(url: getFileUrl(pathToFile: pathToFormDemo) ) )
#endif


func getFileUrl(pathToFile: String) -> URL {
    let url = Bundle.main.url(forResource: "\(pathToFile)", withExtension: "pdf")
    return url!
}

func getTestData() {
    if let filepath = Bundle.main.path(forResource: "testData_clientList", ofType: "txt") {
        do {
            let contents = try String(contentsOfFile: filepath)
            parseTestData(data: contents)
        } catch {
            print(error)
        }
    } else {
        print("testdata.txt not found")
    }
}

func parseTestData(data: String) {
    //  Split by lines, drop the first line label
    var lines = data.split(separator: "\n").dropFirst()
    let labelsLine = lines.first
    let labelsForMapping = labelsLine!.split(separator: ",")
    lines = lines.dropFirst()
//    var fieldMap: [String: Int]
//    for field in labelsForMapping {
//        let fieldIndex = labelsForMapping.firstIndex(of: "\(field)")
//        fieldMap[String(field)] = fieldIndex
//    }
    for item in lines {
        let tokens = item.split(separator: ",")
        let firstNameIndex = labelsForMapping.firstIndex(of: "firstName")
        var client: Client
        if let typeIndex = labelsForMapping.firstIndex(of: "type") {
            client = Client(firstName: String(tokens[firstNameIndex!]), type: Client.ClientType(rawValue: String(tokens[typeIndex])) ?? .Other)
        } else {
            client = Client(firstName: String(tokens[firstNameIndex!]))
        }
        var currIndex = 0
        for field in tokens {
            let f = String(field)
            switch labelsForMapping[currIndex] {
            case "firstName": break
            case "middleName": client.middleName = f
            case "lastName": client.lastName = f
            case "legalFullName": client.legalFullName = f
            case "email_address": client.email_address = f
            case "mobile_phone_number": client.mobile_phone_number = f
            case "type": client.type = Client.ClientType(rawValue: f) ?? .Other
            case "addToGroup":
                if f != "nil" {
                    let groupPrimaryID = g_clients.list.filter { key, value in
                        return value.legalFullName == f
                    }
                    if !groupPrimaryID.isEmpty {
                        client.addToGroup(client: groupPrimaryID.first!.key)
                    } else {
                        print("Failed to add \(client.firstName) to group with \(f)")
                    }
                }
            default:
                print("Field -> label:\(labelsForMapping[currIndex]) value:\(f) for Client -> \(client.firstName) fell through switch and failed to save")
            }
            currIndex += 1
        }
        g_clients.list[client.id] = client
    }
}
