//
//  Property.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/28/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation

class Property: Identifiable {
    let id = UUID()
    var address = Address()
    lazy var lotNum = String()
    lazy var blockNum = String()
    lazy var subdivision = String()
    lazy var county = String()
    lazy var legalDescription = String()
    
    lazy var listPrice = Money()
    
    init() {
    }
}

struct Address {
    var line_1 = String()
    var line_2 = String()
    var City = String()
    var State = String()
    var Zip = String()
}

