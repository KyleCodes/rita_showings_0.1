//
//  Person.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/28/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation

class Person: Identifiable, Hashable {
    static func == (lhs: Person, rhs: Person) -> Bool {
        return lhs.firstName == rhs.firstName && lhs.legalFullName == rhs.legalFullName && lhs.lastName == rhs.lastName
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(legalFullName)
        hasher.combine(firstName)
        hasher.combine(lastName)
    }
    
    let id = UUID()
    var firstName = String()
    lazy var middleName = String()
    lazy var lastName = String()
    lazy var legalFullName = String()
    var email_address: String {
        get {
            return self.email_addresses.first!
        }
        set {
            self.email_addresses.insert(newValue, at: 0)
        }
    }
    func getAllEmails() -> [String] {
        return self.email_addresses
    }
    fileprivate var email_addresses: [String] = [String()]
    lazy var mobile_phone_number = String()
    lazy var additional_phone_numbers: [String] = [String()]
    
    //  Addresses
    var address_home: Address = Address() {
        willSet {
            prev_addresses.append(self.address_home)
            self.address_home = newValue
        }
    }
    var address_work: Address?
    lazy var prev_addresses: [Address] = []
}
