//
//  AgentProfile.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 7/18/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

var currentUser : AgentProfile = AgentProfile(firstName: "Steve", lastName: "Jobs")
let systemUser : AgentProfile = AgentProfile(firstName: "sys", lastName: "usr")

/*
    https://www.reso.org/ouid-api/
    Api to look up organizations
    TODO --> Fill agent and broker data from API
 
    https://www.reso.org/web-api-examples/mls/north-texas-re-info-systems/
    NTREIS WEB API Examples
 
 */
class AgentProfile : Person, Combine.ObservableObject {

    var license_number = String()
    lazy var MLSID : String = {
        if self.license_number.isEmpty == false {
            return self.license_number
        }else { return String() }
    }()
    lazy var commission_rate = String()
    lazy var broker = Broker()
	lazy private var isBroker : Bool = false
	
    init(firstName : String, lastName : String) {
        super.init()
        self.firstName = firstName
        self.lastName = lastName
	}
	
    struct Broker : Identifiable {
        var id : UUID {
            UUID()
        }
        
		var licNum = String()
		var firmName = String()
		var phoneNum = String()
		var principalName = String()
		var address = Address()
	}
    
    func setDefaultsIfEmpty() {
        if self.commission_rate.isEmpty {
            self.commission_rate = "3"
        }
        if self.MLSID.isEmpty {
            self.MLSID = self.license_number
        }
    }
}
