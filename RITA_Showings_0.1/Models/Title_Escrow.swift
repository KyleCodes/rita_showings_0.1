//
//  Title_Escrow.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/29/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation

class Title_Escrow_Company: Identifiable {
    let id = UUID()
    
    var name: String
    var office_address = Address()
    var office_phoneNumber: String?
    lazy var office_name: String = self.name
    
    var branch_manager: Title_Escrow_Agent.ID?
    var branch_escrowAgents: [Title_Escrow_Agent.ID: Title_Escrow_Agent] = [:]
    lazy var branch_phoneNumber = office_phoneNumber
    
    init(name: String) {
        self.name = name
    }
}

class Title_Escrow_Agent: Person {
    var assistant: Person.ID?
}
