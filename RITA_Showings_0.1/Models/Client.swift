//
//  Client.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/28/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import Combine

final class Client: Person, ObservableObject {
    
    // #MEMEBERS
    var type: ClientType
    private var grouped = false
    lazy var isGroupPrimary = false
    lazy var isSigner = true
    @Published var isSelected = false
    
    lazy var isGrouped: (() -> Bool) = {
        return self.grouped ? true : false
    }
    
    var groupedWith: [Client.ID]?
    var spouse: [Client.ID]?
    lazy var disclosueRelatedToLicenseHolder = "N/a"
    
    func addToGroup(client: Client.ID) {
        if !self.isGroupPrimary {
            let primary = g_clients.list[client]!
            primary.isGroupPrimary = true
            primary.groupedWith?.append(self.id)
            primary.grouped = true
            self.grouped = true
            self.groupedWith?.append(client)
        } else {
            print("This client is a Group Primary.  Client's may only be apart of 1 group at a time.")
        }
    }
    
    func removeFromGroup(client: Client.ID) {
        if isGroupPrimary || grouped == false {
            print("This client is the Primary of a group or not apart of a group and cannot be removed.")
        }else{
            self.groupedWith?.removeAll{$0 == client}
            
            print("\(g_clients.list[client]!.firstName) \(g_clients.list[client]!.lastName) was removed from Client Group.")
            if groupedWith!.isEmpty {
                self.grouped = false
            }
        }
    }
    
    func combineAllStringFieldsForSearching() -> String {
        let combinedString = "\(self.firstName)\(self.middleName)\(self.lastName)\(self.legalFullName)\(self.email_address)\(self.mobile_phone_number)"
        return combinedString
    }
    
    init(firstName: String) {
        self.type = .Other
        super.init()
        self.firstName = firstName
    }
    
    init(firstName: String, type: ClientType) {
        self.type = type
        super.init()
        self.firstName = firstName
    }

    enum ClientType: String {
        case Buyer = "Buyer"
        case Seller = "Seller"
        case Buyer_Seller = "Buyer_Seller"
        case Renter = "Renter"
        case Investor = "Investor"
        case Other = "Other"
    }
}
