//
//  Money.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 9/28/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation

struct Money {
    var cents: Int = 0
    
    func asString() -> String {
        let str = String("\(self.asDecimal())")
        return str
    }
    func asDecimal() -> NSDecimalNumber {
        let c = NSDecimalNumber(value: self.cents)
        return c.dividing(by: 100)
    }
    func asDouble() -> Double {
        let c = Double(self.cents)
        return c/100
    }
    
    init() {
        self.cents = 0
    }
    init(cents: Int) {
        self.cents = cents
    }
    init(dollars: Double) {
        self.cents = Int( (dollars * 100).rounded() )
    }
    init(dollars: NSDecimalNumber) {
            self.cents = Int( Double(truncating: (dollars.multiplying(byPowerOf10: 2))).rounded() )
    }
}
