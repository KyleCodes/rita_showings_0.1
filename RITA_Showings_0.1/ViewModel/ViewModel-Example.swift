//
//  ViewModel-Example.swift
//  RITA_Showings_0.1
//
//  Created by Kyle Teixeira on 8/28/19.
//  Copyright © 2019 Teixeira Software Solutions LLC. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

//  Example model view for saving state between SwiftUI View and model
//  using new Combine Framework
//  https://infinum.co/the-capsized-eight/combine-makes-swiftui-shine

final class RepositoryViewModel: BindableObject {
   // BindableObject conformance
   let didChange: AnyPublisher<Void, Never>

   // Public properties
   var repositories: [Repository] = []

   var text: String = "" {
       didSet { _textDidChange.send(text) }
   }

   // Private properties
   private let _textDidChange = PassthroughSubject<String, Never>()

   private var _cancellable: Cancellable?

   // Init
   init(repositories: [Repository] = []) {
       self.repositories = repositories

       let search = _textDidChange
           .filter { !$0.isEmpty }
           .debounce(for: .seconds(0.3), scheduler: DispatchQueue.main)
           .flatMapLatest { RepositoryViewModel._getRepositories(using: $0, existingRepositories: repositories) }
           .share()

       didChange = search
           .map { _ in () }
           .receive(on: DispatchQueue.main)
           .eraseToAnyPublisher()

       _cancellable = search
           .tryMap { try $0.get() }
           .catch { _ in Publishers.Just(repositories) }
           .assign(to: \.repositories, on: self)
   }

   /// Deinit
   deinit {
       _cancellable?.cancel()
   }
}
